
import java.util.Iterator;
import java.util.List;

public class CollisionController {	

	private DrawableContainer container;
	private static CollisionController collisionController;

	private CollisionController(){

	}

	public static CollisionController getInstance(){
		if(collisionController==null){
			collisionController = new CollisionController();
		}
		return collisionController;
	}


	public void DetectCollision(DrawableContainer drawableContainer) {		
		this.container = drawableContainer;
		detectSpaceshipCollision(drawableContainer.getSpaceship(), drawableContainer.getAsteroidHandler().getIterator());
		detectAsteroidDestruction(drawableContainer.getAsteroidHandler().getList(), drawableContainer.getShotHandler().getList());
	}
	
	private void detectSpaceshipCollision(Spaceship spaceship, Iterator<Drawable> asteroidIterator){	
		while(asteroidIterator.hasNext()) {			
			collisionDetection(spaceship,asteroidIterator.next());
		}		
	}
	
	private void detectAsteroidDestruction(List<Drawable> asteroidList, List<Drawable> shotList) {
		/*while(shotIterator.hasNext()){
			while(asteroidIterator.hasNext()){
				if(collisionDetection(asteroidIterator.next(), shotIterator.next())){
					//shotIterator.remove();
					//asteroidIterator.remove();
					System.out.println("peewpew1");
				}
			}
		}*/
		for(int i = 0; i < shotList.size();i++){
			for(int j = 0; j< asteroidList.size();j++){
				if(collisionDetection(asteroidList.get(j), shotList.get(i))){
					asteroidList.remove(j);
					shotList.remove(i);
					container.increaseScore();
					break;
				}
			}
		}


	}
	
	private void collisionDetection(Spaceship spaceship, Drawable asteroid) {
		for(int i = 0; i<3; i++) {
			if(asteroid.intersects(spaceship.getPoints().get(i))){
				System.out.println("Game Over");
				System.exit(0);
			}
		}
	}

	private boolean collisionDetection(Drawable asteroid, Drawable shot) {
		if(asteroid.intersects(shot.getCurrentPoint())){
			return true;
		}
		else{
			return false;
		}
	}
}
