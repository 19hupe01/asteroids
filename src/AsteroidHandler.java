import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AsteroidHandler {

	private static AsteroidHandler asteroidHandler;

	List<Drawable> AsteroidList = new ArrayList<Drawable>(); 
	
	private AsteroidHandler(){

	}

	public static AsteroidHandler getInstance(){
		if(asteroidHandler == null){
			asteroidHandler = new AsteroidHandler();
		}
		return asteroidHandler;
	}

	public Iterator<Drawable> getIterator(){
		return this.AsteroidList.iterator();
	}
	
	public List<Drawable> getList(){
		return this.AsteroidList;
	}
	
	public void removeAsteroid(Drawable a) {
		AsteroidList.remove(a);
	}
	
	public void createNewAsteroid() {
		this.AsteroidList.add(new Asteroid());
		//System.out.println("Asteroid skapad");
	}
	

}
