import javax.swing.text.Position;

public class PositionCalculator {


	private static PositionCalculator positionCalculator;

	private PositionCalculator(){

	}

	public static PositionCalculator getInstance(){
		if(positionCalculator==null){
			positionCalculator = new PositionCalculator();
		}
		return positionCalculator;
	}


	public void calculatePositions(DrawableContainer drawableContainer){

		calculateSpaceshipPosition(drawableContainer.getSpaceship());
		calculateAsteroidPositions(drawableContainer.getAsteroidHandler());
		calculateShotPositions(drawableContainer.getShotHandler());


	}

	private void calculateAsteroidPositions(AsteroidHandler asteroidHandler) {

		for(int i = 0; i < asteroidHandler.getList().size(); i++) {
			asteroidHandler.getList().get(i).setCurrentX(
					asteroidHandler.getList().get(i).getCurrentX()
					+asteroidHandler.getList().get(i).getSpeedX());
			asteroidHandler.getList().get(i).setCurrentY(
					asteroidHandler.getList().get(i).getCurrentY()
					+asteroidHandler.getList().get(i).getSpeedY());
		}
	}
	private void calculateShotPositions(ShotHandler shotHandler) {

		for(int i = 0; i < shotHandler.getList().size(); i++) {
			shotHandler.getList().get(i).setCurrentX(
					shotHandler.getList().get(i).getCurrentX()
					+shotHandler.getList().get(i).getSpeed()
					*10
					* Math.sin(shotHandler.getList().get(i).getAngle()));
			shotHandler.getList().get(i).setCurrentY(
					shotHandler.getList().get(i).getCurrentY()
					+shotHandler.getList().get(i).getSpeed()
					*10
					* -Math.cos(shotHandler.getList().get(i).getAngle()));
		}
	}

	private void calculateSpaceshipPosition(Spaceship spaceship) {

		spaceship.setCurrentX((spaceship.getCurrentX() 
				+ spaceship.getSpeed() 
				* 10 
				* Math.sin(spaceship.getAngle())));

		spaceship.setCurrentY((spaceship.getCurrentY() 
				- spaceship.getSpeed() 
				* 10 
				* Math.cos(spaceship.getAngle())));

		spaceship.calculateSpeed();
		spaceship.update();
	}



}
