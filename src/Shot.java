import java.awt.Color;
import java.awt.Graphics;

public class Shot implements Drawable {
	
	
	private double radius = 0;
	private Point currentPoint = new Point(0,0);
	private double speed = 0;
	private double angle = 0;
	
	public Shot(Point startPoint, double angle) {
		this.currentPoint = startPoint;
		this.angle = angle;
		this.radius = 3;
		this.speed = 1;
	}
	
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval((int)currentPoint.getX(), (int)currentPoint.getY(), (int) radius*2, (int) radius*2);
		
	}
	@Override
	public double getCurrentX() {
		return currentPoint.getX();
	}
	@Override
	public double getCurrentY() {
		return currentPoint.getY();
	}
	@Override
	public void setCurrentX(double x) {
		this.currentPoint.setX(x);
		
	}
	@Override
	public void setCurrentY(double y) {
		this.currentPoint.setY(y);
		
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getSpeed() {
		return this.speed;
	}
	@Override
	public double getRadius() {
		return this.radius;
	}
	
	@Override
	public double getAngle() {
		return this.angle;
	}


	@Override
	public double getSpeedX() {
		return 0;
	}


	@Override
	public double getSpeedY() {
		return 0;
	}
	
	public Point getCurrentPoint() {
		return currentPoint; 
	}

	@Override
	public boolean intersects(Point point) {
		return currentPoint.distanceTo(point) < radius;
	}
}
