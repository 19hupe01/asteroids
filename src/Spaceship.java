import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Spaceship{

	private Point currentPoint = new Point(180,180); 
	private int width = 30;
	private int height = 40;
	private ArrayList<Point> points = new ArrayList<Point>();
	private double angle = 0;
	private double speed = 0;
	private double acceleration = 0;
	
	private boolean rotatingRight = false;
	private boolean rotatingLeft = false;
	
	public Spaceship() {
		//Dom h�r kan vara scuffed, se �ver senare 
		points.add(new Point((currentPoint.getX()+width/2), currentPoint.getY()));
		points.add(new Point(currentPoint.getX(), (currentPoint.getY()+height)));
		points.add(new Point((currentPoint.getX()+width), (currentPoint.getY()+height)));
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int length) {
		this.height = length;
	}
	
	public void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		AffineTransform tx = new AffineTransform();
		tx.rotate(angle,(currentPoint.getX()+width/2),(currentPoint.getY()+height/2));
		
		Shape polygon = new Polygon(this.getxPoints(),this.getyPoints(),3);
		
		//tx.createTransformedShape(polygon);
        
		g2d.setColor(Color.BLUE);

		g2d.draw(tx.createTransformedShape(polygon));		
	}
	public double getCurrentY() {
		return currentPoint.getY();
	}
	public void setCurrentY(double y) {
		this.currentPoint.setY(y);
	}
	public double getCurrentX() {
		return currentPoint.getX();
	}
	public void setCurrentX(double x) {
		this.currentPoint.setX(x);
	}
	
	
	public void accelerate() {
		this.acceleration = 0.015;
	}
	
	public void deAccelerate() {
		this.acceleration = 0;
		
	}
	
	public void calculateSpeed() {
		
		if(speed < 0)
		{
			speed = 0;
		}
		else if(speed > 0 && this.acceleration == 0)
		{
			speed += -0.02;
		}
		else if(speed < 1) {
			speed += acceleration;
		}

	}
	
	public void rotateLeft() {
		rotatingLeft = true;
	}
	public void rotateRight() {
		rotatingRight = true;
	}
	
	public void stopRotatingLeft() {
		rotatingLeft = false;
	}
	public void stopRotatingRight() {
		rotatingRight = false;
	}
	
	
	
	public void update() {
		points.get(0).setX((currentPoint.getX()+width/2));
		points.get(0).setY((currentPoint.getY()));
		points.get(1).setX(currentPoint.getX());
		points.get(1).setY((currentPoint.getY()+height));
		points.get(2).setX((currentPoint.getX()+width));
		points.get(2).setY((currentPoint.getY()+height));
		
		/*this.xPoints[0] = (int) ((currentX+width/2));
		this.xPoints[1] = (int) (currentX);
		this.xPoints[2] = (int) ((currentX+width));
		this.yPoints[0] = (int) (currentY);
		this.yPoints[1] = (int) ((currentY+height));
		this.yPoints[2] = (int) ((currentY+height));*/
		
		if(rotatingLeft && rotatingRight) {
			
		}
		else if(rotatingRight) {
			angle += 0.1;
			if(angle > 3.15)
			{
				angle = -3.15;
			}
		}
		else if(rotatingLeft) {
			angle -= 0.1;
			if(angle < -3.15)
			{
				angle = 3.15;
			}
		}
		
	}
		
	public double getSpeed() {
		return this.speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public double getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	public int[] getyPoints() {
		int[] yPoints = new int[3];
		for(int i = 0; i<3; i++) {
			yPoints[i] = (int)points.get(i).getY();
		}		
		return yPoints;
	}

	public int[] getxPoints() {
		int[] xPoints = new int[3];
		for(int i = 0; i<3; i++) {
			xPoints[i] = (int)points.get(i).getX();
		}		
		return xPoints;
	}
	
	public ArrayList<Point> getPoints(){
		return points;
	}
	
	public double getAngle() {
		return this.angle;
	}
	
	public Point getShootingPoint() {
		return new Point(getPoints().get(0).getX(), getPoints().get(0).getY());
	}
}
