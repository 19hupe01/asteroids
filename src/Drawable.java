import java.awt.Graphics;

public interface Drawable {
	public void draw(Graphics g);
	public double getCurrentX();
	public double getCurrentY();
	public Point getCurrentPoint();
	public void setCurrentX(double x);
	public void setCurrentY(double y);
	public boolean intersects(Point point);
	public double getSpeedX();
	public double getSpeedY();
	public double getRadius();
	public double getAngle();
	public double getSpeed();
	
}
