public class BoundsController {

	private static BoundsController boundsController;

	private BoundsController(){

	}

	public static BoundsController getInstance(){
		if(boundsController==null){
			boundsController = new BoundsController();
		}
		return boundsController;
	}


	public void checkBounds(DrawableContainer drawableContainer) {
		
		checkSpaceshipBounds(drawableContainer.getSpaceship());
		checkAsteroidBounds(drawableContainer.getAsteroidHandler());
		checkShotBounds(drawableContainer.getShotHandler());
	}
	
	private void checkShotBounds(ShotHandler shotHandler) {
		for(int i = 0; i < shotHandler.getList().size(); i++) {
			if(shotHandler.getList().get(i).getCurrentX() < -CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				shotHandler.getList().remove(i);
			}
			else if(shotHandler.getList().get(i).getCurrentX() > CONSTANTS.WINDOW_X_SIZE+CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				shotHandler.getList().remove(i);
			}
			else if(shotHandler.getList().get(i).getCurrentY() > CONSTANTS.WINDOW_Y_SIZE+CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				shotHandler.getList().remove(i);
			}
			else if(shotHandler.getList().get(i).getCurrentY() < -CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				shotHandler.getList().remove(i);
			}
		}
	}

	private void checkSpaceshipBounds(Spaceship spaceship) {
		if(spaceship.getCurrentX() > CONSTANTS.WINDOW_X_SIZE) {
			spaceship.setCurrentX(0);
		}
		else if(spaceship.getCurrentX() < 0)
		{
			spaceship.setCurrentX(CONSTANTS.WINDOW_X_SIZE-spaceship.getWidth());
		}
		
		if(spaceship.getCurrentY() > CONSTANTS.WINDOW_Y_SIZE) {
			spaceship.setCurrentY(0);
		}
		else if(spaceship.getCurrentY() < 0)
		{
			spaceship.setCurrentY(CONSTANTS.WINDOW_Y_SIZE-spaceship.getHeight());
		}
	}
	
	private void checkAsteroidBounds(AsteroidHandler asteroidHandler) {

		for(int i = 0; i < asteroidHandler.getList().size(); i++) {
			if(asteroidHandler.getList().get(i).getCurrentX() < -CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				asteroidHandler.getList().remove(i);
			}
			else if(asteroidHandler.getList().get(i).getCurrentX() > CONSTANTS.WINDOW_X_SIZE+CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				asteroidHandler.getList().remove(i);
			}
			else if(asteroidHandler.getList().get(i).getCurrentY() > CONSTANTS.WINDOW_Y_SIZE+CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				asteroidHandler.getList().remove(i);
			}
			else if(asteroidHandler.getList().get(i).getCurrentY() < -CONSTANTS.OUT_OF_BOUNDS_PIXELS) {
				asteroidHandler.getList().remove(i);
			}
		}
		
	}
	
}
