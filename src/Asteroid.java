import java.awt.Color;
import java.awt.Graphics;

public class Asteroid implements Drawable{

	private double radius = 0;
	private Point currentPoint = new Point(0,0);
	private double speedX = 0;
	private double speedY = 0;
	
	
	@Override
	public void draw(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillOval((int)currentPoint.getX(), (int)currentPoint.getY(), (int) radius*2, (int) radius*2);
	}
	
	public Asteroid(){
		
		int directionType = (int)(((Math.random())*4)+1);
		//System.out.println(directionType);
		
		this.radius = 12+Math.random()*30;

		switch(directionType){
		case 1:
			//From the Top
			
			this.currentPoint.setX(((Math.random() * CONSTANTS.WINDOW_X_SIZE)));
			this.currentPoint.setY(-CONSTANTS.OUT_OF_BOUNDS_PIXELS);
			this.speedX = Math.random()-0.5;
			this.speedY = Math.random()+1.5;
			break;
		case 2:
			//From the Left
			this.currentPoint.setX(0);
			this.currentPoint.setY(((Math.random() * CONSTANTS.WINDOW_Y_SIZE)));
			this.speedX = Math.random()+1.5;
			this.speedY = Math.random();
			break;
		case 3:
			//From the Right
			this.currentPoint.setX(((CONSTANTS.WINDOW_Y_SIZE + CONSTANTS.OUT_OF_BOUNDS_PIXELS)));
			this.currentPoint.setY(((Math.random() * CONSTANTS.WINDOW_Y_SIZE)));
			this.speedX = Math.random()-1.5;
			this.speedY = Math.random();
			break;
		case 4:
			//From the Bottom		
			this.currentPoint.setX(((Math.random() * CONSTANTS.WINDOW_X_SIZE)));
			this.currentPoint.setY(((CONSTANTS.OUT_OF_BOUNDS_PIXELS + CONSTANTS.WINDOW_Y_SIZE) - 10));
			this.speedX = Math.random()-0.5;
			this.speedY = Math.random()-1.5;
			break;			
		}			
	}
	
	public double getSpeedX() {
		return this.speedX;
	}
	public double getSpeedY() {
		return this.speedY;
	}

	@Override
	public double getCurrentX() {
		return this.currentPoint.getX();
	}

	@Override
	public double getCurrentY() {
		return this.currentPoint.getY();
	}

	@Override
	public void setCurrentX(double x) {
		this.currentPoint.setX(x);
	}

	@Override
	public void setCurrentY(double y) {
		this.currentPoint.setY(y);
		
	}

	@Override
	public double getRadius() {
		return this.radius;
	}

	@Override
	public double getAngle() {
		return 0;
	}

	@Override
	public double getSpeed() {
		return 0;
	}

	@Override
	public boolean intersects(Point point) {
	    Point center = new Point(currentPoint.getX()+radius, currentPoint.getY()+radius);
	    return center.distanceTo(point) < radius;
	}

	@Override
	public Point getCurrentPoint() {
		return currentPoint;
	}
}
