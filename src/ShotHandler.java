import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShotHandler {

	List<Drawable> ShotList = new ArrayList<Drawable>(); 
	
	public Iterator<Drawable> getIterator(){
		return this.ShotList.iterator();
	}
	
	public List<Drawable> getList(){
		return this.ShotList;
	}
	
	public void removeShot(Drawable s) {
		ShotList.remove(s);
	}
	
	public void createNewShot(Point startPoint, double angle) {
		this.ShotList.add(new Shot(startPoint,angle));
	}
	
}
