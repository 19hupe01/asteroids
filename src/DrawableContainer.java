import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Iterator;

import javax.swing.JPanel;

public class DrawableContainer extends JPanel{
	
	private static final long serialVersionUID = 1L;
	
	private Spaceship spaceship;
	private AsteroidHandler asteroidHandler;
	private ShotHandler shotHandler;
	private int score = 0;
	
	private static DrawableContainer drawableContainer;

	private DrawableContainer(){
		this.spaceship = new Spaceship();
		this.asteroidHandler = AsteroidHandler.getInstance();
		this.shotHandler = new ShotHandler();
		repaint();
	}

	public static DrawableContainer getInstance(){
		if(drawableContainer==null){
			drawableContainer = new DrawableContainer();
		}
		return drawableContainer;
	}
	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		this.spaceship.draw(g);
		
		
		Iterator<Drawable> asteroidDrawIterator = asteroidHandler.getIterator();
		Iterator<Drawable> shotDrawIterator = shotHandler.getIterator();
		
		
		while(asteroidDrawIterator.hasNext()) {
			asteroidDrawIterator.next().draw(g);
		}
		while(shotDrawIterator.hasNext()) {
			shotDrawIterator.next().draw(g);
		}
		g.setFont(new Font("Courier New", 1, 30));
		g.setColor(Color.black);
		g.drawString(String.valueOf(score), 50, 50);
	}
	
	public void increaseScore(){
		this.score++;
	}

	public Spaceship getSpaceship() {
		return this.spaceship;
	}
	
	public AsteroidHandler getAsteroidHandler() {
		return this.asteroidHandler;
	}
	
	public ShotHandler getShotHandler() {
		return this.shotHandler;
	}
	
	public void createNewShot() {
		this.shotHandler.createNewShot(this.spaceship.getShootingPoint(), this.getSpaceship().getAngle());
	}
	public void createNewAsteroid() {
		this.asteroidHandler.createNewAsteroid();
	}
	
}
