
public class CONSTANTS {

	//60 FPS
	final static int LOOP_SPEED = 30;
	
	//Window size
	final static double WINDOW_X_SIZE = 600.0;
	final static double WINDOW_Y_SIZE = 600.0;
	
	//Chance to spawn an Asteroid every loop
	final static double ASTEROID_SPAWNRATE = 0.03;
	
	final static int OUT_OF_BOUNDS_PIXELS = 10;
}
