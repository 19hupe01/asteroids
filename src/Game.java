import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

public class Game extends JFrame implements KeyListener{

	private DrawableContainer drawableContainer;
	private Timer timer;
	private PositionCalculator positionCalculator;
	private BoundsController boundsController;
	private CollisionController collisionController;
	
	
	private static final long serialVersionUID = 1L;

	public Game() {
		this.positionCalculator = PositionCalculator.getInstance();
		this.collisionController = CollisionController.getInstance();
		this.boundsController = BoundsController.getInstance();
		this.drawableContainer = DrawableContainer.getInstance();
		this.add(drawableContainer);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize((int)CONSTANTS.WINDOW_X_SIZE,(int)CONSTANTS.WINDOW_Y_SIZE);
		this.setVisible(true);
		this.addKeyListener(this);
		this.timer = new Timer();
		
	}
	
	public void start(){
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				Gameloop();	
			}
			
		},0,CONSTANTS.LOOP_SPEED);
		
		
	}

	private void Gameloop() {
		
		
		boundsController.checkBounds(this.drawableContainer);
		positionCalculator.calculatePositions(this.drawableContainer);
		
		collisionController.DetectCollision(this.drawableContainer);		
		
		if(Math.random() > 1-CONSTANTS.ASTEROID_SPAWNRATE)
		{
			this.drawableContainer.createNewAsteroid();
		}
		
		this.drawableContainer.repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent event) {
        if(event.getKeyChar() == 'a') {
        	this.drawableContainer.getSpaceship().rotateLeft();
        }
        if(event.getKeyChar() == 'd') {
        	this.drawableContainer.getSpaceship().rotateRight();
        }
        if(event.getKeyChar() == 'w') {
        	this.drawableContainer.getSpaceship().accelerate();
        }
        if(event.getKeyChar() == ' ') {
        	this.drawableContainer.createNewShot();
        }
		
	}

	@Override
	public void keyReleased(KeyEvent event) {
		if(event.getKeyChar() == 'a') {
        	this.drawableContainer.getSpaceship().stopRotatingLeft();
        }
		if(event.getKeyChar() == 'd') {
        	this.drawableContainer.getSpaceship().stopRotatingRight();
        }
		if(event.getKeyChar() == 'w') {
        	this.drawableContainer.getSpaceship().deAccelerate();
        }
		
	}
}
